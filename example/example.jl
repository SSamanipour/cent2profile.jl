using Cent2Profile
using MS_Import

##

path="path/to/the/file"

filenames=["test_file.mzXML"]
mz_thresh=[200,300]
res = 20000


chrom=import_files(path,filenames,mz_thresh)
mz_val = chrom["MS1"]["Mz_values"]
mz_int = chrom["MS1"]["Mz_intensity"]

mz_val_cent,mz_int_cent,dm_c = centroid(mz_val,mz_int,min_int,res)

mz_prof_pred,int_prof_pred=cent2profile(mz_val_cent,mz_int_cent)