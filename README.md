# Cent2Profile

[![Build Status](https://ci.appveyor.com/api/projects/status/github/saersamani/Cent2Profile.jl?svg=true)](https://ci.appveyor.com/project/saersamani/Cent2Profile-jl)



 **Cent2Profile.jl** is a julia package for seamless conversion of centrioded and profile HRMS data. The algorithm is capable of converting profile HRMS data to centrodied using a self-adjusting approach similar to [**SAFD.jl**](https://bitbucket.org/SSamanipour/safd.jl/src/master/). For the conversion from centroided to profile, a random forest regression model trained with 1e13 scans generated by three different vendors. Please note that the model file is very large. Therefore, the installation might take several minutes, depending on the internet speed. 

## Installation

Given that **CompCreate.jl** is not a registered julia package, for installation the *url* to the repository and the julia package manager are necessary.

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/cent2profile.jl/src/master/"))
Pkg.test("Cent2Profile")


```

## Usage
There are two main functions implemented via **Cent2Profile.jl**, namely: *centroid(--)* and *cent2profile(--)*. These functions are able to work with the output from [**MS_Import.jl**](https://bitbucket.org/SSamanipour/ms_import.jl/src/master/).

###  centroid(--)
This function performs the centroiding of the profile HRMS data.
```julia
using Cent2Profile
using MS_import

mz_values,mz_int,t0,t_end,file_name,pathin,
msModel,msIonisation,msManufacturer,polarity,Rt=import_files_MS1(pathin::String,filenames::Array{String,2}) # please check the manual for [**MS_Import.jl**](https://bitbucket.org/SSamanipour/ms_import.jl/src/master/)

mz_val_cent,mz_int_cent,dm_c = centroid(mz_val::Array{Float64,2},mz_int::Array{Float64,2},min_int::Int64,res::Int64)


```
#### Inputs centroid(--)


# Needed inputs

* **mz_val::Array{Float64,2}** is an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
* **mz_int::Array{Float64,2}** is an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
* **min_int::Int64 (0-Inf)** is the minimum intensity of the ions to be considered meaningful (i.e. not noise).
* **res::Int64 (15000-120000)** is the nominal resolution of the mass spectrometer. 

# Optional inputs 

* **r_thresh::Float64 (0-1)** is the correlation coefficient used to assess if the signal is Gaussian shaped or not. The default value of this parameter is 0.8.
* **min_p_w::Float64 (0.001-0.1)** is the minimum mass peakwidth that is used as a first guess by the algorithm. The defualt value of 0.02 Da is set based on a nominal resolution of 20000. 
* **s2n::Float64 (1-10)** is the ratio of the median of the signal in the selected window to the maximum signal. The defualt value is set to 1.5.   

#### Output centroid(--)

This function generates three Arrays including the masses, the intensities, and the peakwidths of each detected signal in the data. 


### cent2profile(--)
This function transforms the centroided data to profile one.

```julia
using Cent2Profile

mz_prof_pred,int_prof_pred = cent2profile(mz_val_cent,mz_int_cent)

```




#### Inputs comp_DIA(--)

# Needed inputs

* **mz_val_cent::Array{Float64,2}** is an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the m/z values.
* **mz_int::Array{Float64,2}** is an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the predicted intensity profiles associated with each m/z value. 

# Optional inputs 

* **fun::String** is to define the type of distribution to be used for profile prediction. At the moment there two different distributions (i.e. Gaussian ("gs") and Cauchy ("scd")) available for the profile predictions. The Gaussian is the defualt setting of the algorithm.


### Examples

For more examples on how to process your own data please take a look at folder [examples](https://bitbucket.org/SSamanipour/cent2profile.jl/master/examples/).


## Contribution

Issues and pull requests are welcome! Please contact the developers for further information.